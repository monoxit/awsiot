/* ESP8266 + DHT11 AWS IoT (MQTT) Shadow sample sketch.
  This sample sketch is based on ESP8266 AWS IoT example by Evandro Luis Copercini
    Public Domain - 2017

  Copyright (c) 2019 Masami Yamakawa MONOxIT Inc.
  Released under the MIT license
  https://opensource.org/licenses/mit-license.php

  To increase MQTT maximum packet size to 512 byte, pubsubclient library is distributed 
  with this sketch. Refer LICENSE_PUBSUBCLIENT_LIBRARY.txt for the license of the library.

  AWS IoTのクライアント証明による認証を使用しています。
  あらかじめ作成したDER形式のクライアント証明とプライベートキー, CAルート証明の３つのファイルを
  スケッチのフォルダのdataフォルダ内に保存し, ESP8266 Sketch Data Uploadプラグインで
  ESP8266のファイルシステムに保存します.

  Arduino IDEでファイル名「secret.h」の新規タブを作成し次の３つの定義を追加します.
  WIFI_SSID, WIFI_PSWD, endpoint...の部分は適切に変更します.

  #define YOUR_WIFI_SSID  "WIFI_SSID"
  #define YOUR_WIFI_PASWD "WIFI_PSWD"
  #define AWS_IOT_API_ENDPOINT "endpoint123456-ats.iot.ap-northeast-1.amazonaws.com"

*/

// このスケッチで使用する各種ライブラリの取り込み
#include <FS.h>
#include <ESP8266WiFi.h>
#include "PubSubClient.h"
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>
#include <DHT.h>
#include "secret.h"

// ネットワーク環境に合わせて要変更
const char* ssid = YOUR_WIFI_SSID;
const char* password = YOUR_WIFI_PASWD;

// AWS IoT接続関連の定義 mqttServerやトピックを適切に変更
const char* mqttServer = AWS_IOT_API_ENDPOINT;
const int   mqttPort = 8883;
const char* mqttPubTopic = "$aws/things/mything/shadow/update";
const char* mqttSubTopic = "$aws/things/mything/shadow/update/delta";
const char* mqttDeviceId = "mything";

// ESP8266 フラッシュメモリ内に別途保存のSSL接続に使う３つのファイル
const char* certFile = "/cert.der"; // クライアント証明書
const char* privateFile = "/private.der"; // 秘密鍵
const char* caFile = "/ca.der"; // ルートCA証明書

// LEDとセンサが接続されているピン番号
const int   ledPin1 = 13;
const int   ledPin2 = 14;
const int   dht11Pin = 5;
const int   dhtType = DHT11;

// 各種ライブラリをスケッチで使うことができる状態にする
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");
// TLS(SSL)対応のWiFiClientSecureライブラリを使う
WiFiClientSecure espClient;
PubSubClient client(espClient);

// JSON形式データを扱うためのメモリを確保
StaticJsonDocument<512> jsonMessage;

// DHT11センサライブラリをdht11という名前で使うようにする
DHT dht11(dht11Pin, dhtType);

int     lastReportedLed1Status;
int     lastReportedLed2Status;
int     currentLed1Status;
int     currentLed2Status;
unsigned long  lastMsg = -30000L;
unsigned long lastLedStatusUpdateMillis = 0;

// AWS IoTからメッセージを受信したときに実行される部分
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // 受信したペイロードのJSONテキストを展開
  DeserializationError err = deserializeJson(jsonMessage, payload);
  if (err) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(err.c_str());
  } else { // 展開できたら
    // 受信したテキスト内にled1 と led2のラベルがあるかどうかを確認
    JsonVariant led1 = jsonMessage["state"]["led1"];
    JsonVariant led2 = jsonMessage["state"]["led2"];
    if (!led1.isNull()) { // led1が存在したら
      // 受信したled1の期待値（0か1）を取得し, ledStatus変数へ代入
      int ledStatus = jsonMessage["state"]["led1"];
      Serial.print("LED1 Status:");
      Serial.println(ledStatus);
      // led1の今の状態をcurrentLed1Status変数に代入しておいて記憶
      currentLed1Status = ledStatus;
      digitalWrite(ledPin1, ledStatus); //受信したledの状態でLEDをON/OFF
    }
    if (!led2.isNull()) {
      int ledStatus = jsonMessage["state"]["led2"];
      Serial.print("LED2 Status:");
      Serial.println(ledStatus);
      currentLed2Status = ledStatus;
      digitalWrite(ledPin2, ledStatus);
    }
  }
}

void setupWifi() {

  delay(10);
  // 無線LANへ接続
  espClient.setBufferSizes(512, 512);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // NTP クライアントライブラリで時刻を更新
  timeClient.begin();
  while (!timeClient.update()) {
    timeClient.forceUpdate();
  }
  // 1970年1月1日0時からの経過秒数の形で現在日時を取得し証明書検証のためにセット
  espClient.setX509Time(timeClient.getEpochTime());

}


void reconnect() {
  // MQTTブローカに接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // MQTTブローカに接続し接続できたら
    if (client.connect(mqttDeviceId)) {
      Serial.println("connected");
      client.subscribe(mqttSubTopic);// 受信したいトピックに申し込み
      delay(100); // 申し込み処理の完了を100ミリ秒待つ

      //現在のLEDの状態をAWS IoTへレポート（パブリッシュ）
      String payload = "{\"state\":{\"reported\":{\"led1\":";
      payload += currentLed1Status;
      payload += ", \"led2\":";
      payload += currentLed2Status;
      payload += "}}}";
      client.publish(mqttPubTopic, payload.c_str());
      Serial.print(mqttPubTopic);
      Serial.print(":");
      Serial.println(payload);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");

      char buf[256];
      espClient.getLastSSLError(buf, 256);
      Serial.print("WiFiClientSecure SSL error: ");
      Serial.println(buf);

      // 接続できないときは5秒待つ
      delay(5000);
    }
  }
}

void setup() {
  WiFi.mode(WIFI_STA);
  Serial.begin(115200);
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);

  delay(1000);
  if (!SPIFFS.begin()) {
    Serial.println("Failed to mount file system");
    return;
  }
  // メモリの使用状況をシリアルモニタへ出力
  Serial.print("Heap: "); Serial.println(ESP.getFreeHeap());

  // 証明書ファイルの読み込み
  File cert = SPIFFS.open(certFile, "r");
  if (!cert) {
    Serial.println("Failed to open cert file");
  }
  else
    Serial.println("Success to open cert file");

  delay(1000);
  // クライアントが証明書を使うようにする
  if (espClient.loadCertificate(cert))
    Serial.println("cert loaded");
  else
    Serial.println("cert not loaded");

  // 秘密鍵ファイルの読み込み
  File private_key = SPIFFS.open(privateFile, "r");
  if (!private_key) {
    Serial.println("Failed to open private cert file");
  }
  else
    Serial.println("Success to open private cert file");

  delay(1000);

  if (espClient.loadPrivateKey(private_key))
    Serial.println("private key loaded");
  else
    Serial.println("private key not loaded");

  // ルートCA証明書ファイルの読み込み
  File ca = SPIFFS.open(caFile, "r");
  if (!ca) {
    Serial.println("Failed to open ca ");
  }
  else
    Serial.println("Success to open ca");

  delay(1000);

  if (espClient.loadCACert(ca))
    Serial.println("ca loaded");
  else
    Serial.println("ca failed");
  // WiFiにつなぐ部分を呼び出す
  setupWifi();
  // MQTTクライアントライブラリにブローカアドレスやポートなどの設定をする
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
  // 最後にAWS IoTへ報告したLEDの状態を0にする
  lastReportedLed1Status = 0;
  lastReportedLed2Status = 0;
  // LEDを消灯する
  digitalWrite(ledPin1, 0);
  digitalWrite(ledPin2, 0);
  Serial.print("Heap: "); Serial.println(ESP.getFreeHeap());
  // DHT11との通信を開始できる状態にする
  dht11.begin();
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  // now変数にマイコン起動時からの経過時間（ミリ秒）を代入
  unsigned long now = millis();
  // もし前回AWS IoTにレポートしてから3秒以上経過していたら
  if (now - lastLedStatusUpdateMillis > 3000) {
    String ledValues = "";
    // もし現在のLED1の状態が前回レポート時と異なっていたら
    if (currentLed1Status != lastReportedLed1Status) {
      // 前回報告のLED1の状態を更新
      lastReportedLed1Status = currentLed1Status;
      // ledValues文字列にJSONのled1の項目（"led1:0"か"led1:1"）を追加
      ledValues += "\"led1\": ";
      ledValues += currentLed1Status;
    }
    if (currentLed2Status != lastReportedLed2Status) {
      lastReportedLed2Status = currentLed2Status;
      if (ledValues != "") ledValues += ",";
      ledValues += "\"led2\": ";
      ledValues += currentLed2Status;
    }
    // もしledValues文字列が空欄でなければ
    if (ledValues != "") {
      // 最後にAWS IoTにレポートした経過時間を更新
      lastLedStatusUpdateMillis = now;
      // AWS IoTのシャドウにパブリッシュするペイロードを作成
      String payload = "{\"state\":{\"reported\":{";
      payload += ledValues;
      payload += "}}}";
      // シャドウへパブリッシュ
      client.publish(mqttPubTopic, payload.c_str());
      Serial.print(mqttPubTopic);
      Serial.print(":");
      Serial.println(payload);
    }
  }
  // 前回温度湿度センサから値を読み取ってから１分以上経過したら
  if (now - lastMsg > 60 * 1000) {
    // 前回読み取り経過時間を更新
    lastMsg = now;
    // センサー値を読み込む（小数点付きの数値）
    float h = dht11.readHumidity();
    float t = dht11.readTemperature();
    // もし読み取り不良なら
    if (isnan(h) || isnan(t)) {
      Serial.print("Failed to read from DHT sensor!");
    } else { //そうでないなら

      // 小数点以下切り捨てて整数のondo, situdoに代入
      int ondo = t;
      int situdo = h;
      
      // シャドウにパブリッシュするペイロード作成
      String payload = "{\"state\":{\"reported\":{\"ondo\": ";
      payload += ondo;
      payload += ",\"situdo\": ";
      payload += situdo;
      payload += "}}}";
      // シャドウにパブリッシュ
      client.publish(mqttPubTopic, payload.c_str());
      Serial.print(mqttPubTopic);
      Serial.print(":");
      Serial.println(payload);
    }
    Serial.print("Heap: "); Serial.println(ESP.getFreeHeap());
  }
}
